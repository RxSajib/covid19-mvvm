package com.example.covid19_mvvm.Screen;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covid19_mvvm.R;
import com.example.covid19_mvvm.Viewmodel.CovidViewModel;
import com.example.covid19_mvvm.model.CountryModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;
import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class Summery extends Fragment  {

    private TextView TotalCases, Deaths, Recovered;
    private CovidViewModel covidViewModel;
    private PieChart pieChart;



    public Summery() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.summery, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        init_view(view);
        getdatafrom_server();
    }

    private void init_view(View view){

        pieChart = view.findViewById(R.id.PieChart);




        TotalCases = view.findViewById(R.id.TotalCaseTextID);
        Deaths = view.findViewById(R.id.DeathsTextID);
        Recovered = view.findViewById(R.id.RecoveredTextID);



    }

    private void getdatafrom_server(){
        covidViewModel = new ViewModelProvider(this).get(CovidViewModel.class);

        covidViewModel.getbangladeshinfo().observe(getViewLifecycleOwner(), new Observer<CountryModel>() {
            @Override
            public void onChanged(CountryModel countryModel) {

                TotalCases.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getCases())));
                Deaths.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getTodayDeaths())));
                Recovered.setText(String.valueOf(String.format(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getRecovered()))));





                pieChart.addPieSlice(new PieModel("Today Cases", countryModel.getTodayCases(), Color.parseColor("#66bb6a")));
                pieChart.addPieSlice(new PieModel("Today Deaths", countryModel.getDeaths() , Color.parseColor("#e53935")));
                pieChart.addPieSlice(new PieModel("Active", countryModel.getCasesPerOneMillion(), Color.parseColor("#FFBB86FC")));
                pieChart.addPieSlice(new PieModel("Critical", countryModel.getDeathsPerOneMillion(), Color.parseColor("#FF03DAC5")));
                pieChart.startAnimation();




                CountryModel mycountrymodel = new CountryModel(countryModel.getCases(), countryModel.getTodayCases(),
                        countryModel.getDeaths(), countryModel.getTodayDeaths(), countryModel.getRecovered(),
                        countryModel.getDeathsPerOneMillion(), countryModel.getDeathsPerOneMillion());
                countryModel.setDeaths(countryModel.getTodayDeaths());

               // covidViewModel.insertcountrydata(mycountrymodel);

            }
        });


        /*
        covidViewModel.getcountrydata().observe(getViewLifecycleOwner(), new Observer<CountryModel>() {
            @Override
            public void onChanged(CountryModel countryModel) {
                if(countryModel != null){
                    TotalCases.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getCases())));
                    Deaths.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getTodayDeaths())));
                    Recovered.setText(String.valueOf(String.format(NumberFormat.getNumberInstance(Locale.US).format(countryModel.getRecovered()))));

//1



                    pieChart.addPieSlice(new PieModel("Today Cases", countryModel.getTodayCases(), Color.parseColor("#66bb6a")));
                    pieChart.addPieSlice(new PieModel("Today Deaths", countryModel.getDeaths() , Color.parseColor("#e53935")));
                    pieChart.addPieSlice(new PieModel("Active", countryModel.getCasesPerOneMillion(), Color.parseColor("#FFBB86FC")));
                    pieChart.addPieSlice(new PieModel("Critical", countryModel.getDeathsPerOneMillion(), Color.parseColor("#FF03DAC5")));
                    pieChart.startAnimation();

                }
            }
        });

        */
    }


}