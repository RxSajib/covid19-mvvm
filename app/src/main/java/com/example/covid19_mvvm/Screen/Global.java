package com.example.covid19_mvvm.Screen;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.covid19_mvvm.Adapter.CountryAdapter;
import com.example.covid19_mvvm.R;
import com.example.covid19_mvvm.Viewmodel.CovidViewModel;
import com.example.covid19_mvvm.model.CountryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class Global extends Fragment {

    private CovidViewModel covidViewModel;
    private RecyclerView recyclerView;
    private CountryAdapter countryAdapter;
    private List<CountryModel> countryModelList = new ArrayList<>();
    private ProgressBar progressBar;
    private LinearLayout Shimmerlayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    public Global() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.global, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.SweapRefreshID);
        Shimmerlayout = view.findViewById(R.id.ShimmerAnim);
        recyclerView = view.findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        countryAdapter = new CountryAdapter();
        recyclerView.setAdapter(countryAdapter);


        covidViewModel = new ViewModelProvider(this).get(CovidViewModel.class);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getdata_server();
            }
        });

        getdata_server();
        search_data();
    }

    private void search_data() {
        covidViewModel.search_country("india").observe(getViewLifecycleOwner(), new Observer<CountryModel>() {
            @Override
            public void onChanged(CountryModel countryModel) {
                Log.d("country", countryModel.getCountry());

            }
        });
    }

    private void getdata_server() {
        covidViewModel.getallcountrydata().observe(getViewLifecycleOwner(), new Observer<List<CountryModel>>() {
            @Override
            public void onChanged(List<CountryModel> countryModels) {

                if (countryModels != null) {

                    covidViewModel.insertglobaldata(countryModels);
                    swipeRefreshLayout.setRefreshing(false);

                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        });


        getallcountrydata();
    }

    private void getallcountrydata() {
        covidViewModel.getalldata().observe(getViewLifecycleOwner(), new Observer<List<CountryModel>>() {
            @Override
            public void onChanged(List<CountryModel> countryModels) {
                if (countryModels != null) {
                    countryAdapter.setCountryModelList(countryModels);
                    countryAdapter.notifyDataSetChanged();
                    Shimmerlayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }
}