package com.example.covid19_mvvm.model;

import java.util.List;

public class CountryModelResponse {

    public List<CountryModel> countryModels;

    public List<CountryModel> getCountryModels() {
        return countryModels;
    }

    public void setCountryModels(List<CountryModel> countryModels) {
        this.countryModels = countryModels;
    }
}
