package com.example.covid19_mvvm.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "GlobaldataDB")
public class GlobalModel {

    @PrimaryKey(autoGenerate = true)
    private int ID;


    @SerializedName("cases")
    private int cases;

    @SerializedName("deaths")
    private int deaths;

    @SerializedName("recovered")
    private int recovered;


    public int getCases() {
        return cases;
    }

    public void setCases(int cases) {
        this.cases = cases;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getRecovered() {
        return recovered;
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
