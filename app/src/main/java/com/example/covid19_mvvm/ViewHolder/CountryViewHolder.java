package com.example.covid19_mvvm.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19_mvvm.Adapter.CountryAdapter;
import com.example.covid19_mvvm.R;

import org.jetbrains.annotations.NotNull;

public class CountryViewHolder extends RecyclerView.ViewHolder {

    public TextView Country, deaths, todaycase,recovered,totalcase;

    public CountryViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        Country = itemView.findViewById(R.id.CountryNameID);
        todaycase = itemView.findViewById(R.id.TodayCaseTextID);
        recovered = itemView.findViewById(R.id.RecoveredTextID);
        totalcase = itemView.findViewById(R.id.TotalCaseTextID);
        deaths = itemView.findViewById(R.id.DeathsTextID);
    }
}
