package com.example.covid19_mvvm.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19_mvvm.R;
import com.example.covid19_mvvm.ViewHolder.CountryViewHolder;
import com.example.covid19_mvvm.model.CountryModel;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> {

    private List<CountryModel> countryModelList;

    public void setCountryModelList(List<CountryModel> countryModelList) {
        this.countryModelList = countryModelList;
    }

    @NonNull
    @NotNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_record, parent, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CountryViewHolder holder, int position) {
        holder.Country.setText(countryModelList.get(position).getCountry());
        holder.deaths.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.ENGLISH).format(countryModelList.get(position).getTodayDeaths())));
        holder.totalcase.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.ENGLISH).format(countryModelList.get(position).getCases())));
        holder.recovered.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.ENGLISH).format(countryModelList.get(position).getRecovered())));
        holder.todaycase.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.ENGLISH).format(countryModelList.get(position).getTodayCases())));

    }

    @Override
    public int getItemCount() {
        if(countryModelList == null){
            return 0;
        }
        else {
            return countryModelList.size();
        }
    }
}
