package com.example.covid19_mvvm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.covid19_mvvm.Screen.Global;
import com.example.covid19_mvvm.Screen.Protection;
import com.example.covid19_mvvm.Screen.Summery;
import com.example.covid19_mvvm.Screen.Symptoms;
import com.example.covid19_mvvm.Viewmodel.CovidViewModel;

public class HomePage extends AppCompatActivity {

    private TextView Summery, Symptoms, Protection, Global;
    private CovidViewModel globalViewmodel;
    private View Summerydot, Symptomsdot, Protectiondot, Globaldot;
    private boolean IsSummary=true, IsSymptom=true, IsProtection=true, IsGlobal=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init_view();

        goto_summerypage(new Summery());
        Summerydot.setVisibility(View.VISIBLE);
    }

    private void trasprent_statusbar(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){

        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init_view(){

        Summerydot = findViewById(R.id.SunneryDot);
        Symptomsdot = findViewById(R.id.SymptomsDot);
        Protectiondot = findViewById(R.id.ProtectionDot);
        Globaldot = findViewById(R.id.GlobalDot);

        Symptoms = findViewById(R.id.SymptomsButtonID);
        Symptoms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(IsSymptom){
                    IsSymptom = false;
                    IsSummary = true;
                    IsProtection = true;
                    IsGlobal = true;
                    Summerydot.setVisibility(View.GONE);
                    Symptomsdot.setVisibility(View.VISIBLE);
                    Protectiondot.setVisibility(View.GONE);
                    Globaldot.setVisibility(View.GONE);
                    goto_symptomspage(new Symptoms());
                }

            }
        });

        Protection = findViewById(R.id.ProtectionID);
        Protection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(IsProtection){
                    IsSymptom = true;
                    IsSummary = true;
                    IsProtection = false;
                    IsGlobal = true;
                    Summerydot.setVisibility(View.GONE);
                    Symptomsdot.setVisibility(View.GONE);
                    Protectiondot.setVisibility(View.VISIBLE);
                    Globaldot.setVisibility(View.GONE);
                    goto_protection_page(new Protection());
                }

            }
        });

        Summery = findViewById(R.id.SummeryButtonID);
        Summery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IsSummary){
                    IsSymptom = true;
                    IsSummary = false;
                    IsProtection = true;
                    IsGlobal = true;
                    Summerydot.setVisibility(View.VISIBLE);
                    Symptomsdot.setVisibility(View.GONE);
                    Protectiondot.setVisibility(View.GONE);
                    Globaldot.setVisibility(View.GONE);
                    goto_summerypage(new Summery());
                }

            }
        });

        Global = findViewById(R.id.GlobalID);
        Global.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IsGlobal){
                    IsSymptom = true;
                    IsSummary = true;
                    IsProtection = true;
                    IsGlobal = false;
                    Summerydot.setVisibility(View.GONE);
                    Symptomsdot.setVisibility(View.GONE);
                    Protectiondot.setVisibility(View.GONE);
                    Globaldot.setVisibility(View.VISIBLE);
                    goto_globalpage(new Global());
                }

            }
        });
    }


    private void goto_symptomspage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragementID, fragment);
            transaction.commit();
        }
    }

    private void goto_protection_page(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragementID, fragment);
            transaction.commit();
        }
    }

    private void goto_summerypage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragementID, fragment);
            transaction.commit();
        }
    }

    private void goto_globalpage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragementID, fragment);
            transaction.commit();
        }
    }
}