package com.example.covid19_mvvm.Api;


import com.example.covid19_mvvm.Network.CountyRespository;
import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.CountryModelResponse;
import com.example.covid19_mvvm.model.GlobalModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CovidApi {


    @GET("all")
    Call<GlobalModel> getglobalcase();

    @GET("countries/bangladesh")
    Call<CountryModel> getbangladeshinfo();


    @GET("countries")
    Call<List<CountryModel>> getallcountryinfo();

    @GET("countries/{countryname}")
    Call<CountryModel> serarch_country(
            @Path("countryname") String name
    );


}
