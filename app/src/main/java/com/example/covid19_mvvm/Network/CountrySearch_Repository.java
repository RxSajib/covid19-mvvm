package com.example.covid19_mvvm.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.covid19_mvvm.Api.CovidApi;
import com.example.covid19_mvvm.ApiClint.CovidClint;
import com.example.covid19_mvvm.model.CountryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountrySearch_Repository {

    private Application application;
    private MutableLiveData<CountryModel> data;
    private CovidApi api;

    public CountrySearch_Repository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        api = CovidClint.getRetrofit().create(CovidApi.class);
    }

    public LiveData<CountryModel> getsearchdata(String name){
        api.serarch_country(name).enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }

}
