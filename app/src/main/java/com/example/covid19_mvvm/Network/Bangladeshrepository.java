package com.example.covid19_mvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.example.covid19_mvvm.Api.CovidApi;
import com.example.covid19_mvvm.ApiClint.CovidClint;
import com.example.covid19_mvvm.model.CountryModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Bangladeshrepository {

    private Application application;
    private MutableLiveData<CountryModel> data;
    private CovidApi globalApis;


    public Bangladeshrepository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        globalApis = CovidClint.getRetrofit().create(CovidApi.class);
    }

    public MutableLiveData<CountryModel> getData(){
        globalApis.getbangladeshinfo().enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(@Nullable Call<CountryModel> call, @Nullable Response<CountryModel> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }
                else {
                    Toast.makeText(application, response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<CountryModel> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }


        });


        return data;
    }

}
