package com.example.covid19_mvvm.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.example.covid19_mvvm.Api.CovidApi;
import com.example.covid19_mvvm.ApiClint.CovidClint;
import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.CountryModelResponse;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountyRespository {

    private Application application;
    private MutableLiveData<List<CountryModel>> data;
    private CovidApi covidApi;


    public CountyRespository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        covidApi = CovidClint.getRetrofit().create(CovidApi.class);

    }

    public MutableLiveData<List<CountryModel>> getallcountrydata(){

        covidApi.getallcountryinfo().enqueue(new Callback<List<CountryModel>>() {
            @Override
            public void onResponse(Call<List<CountryModel>> call, Response<List<CountryModel>> response) {

                if(response.isSuccessful()){
                    data.setValue(response.body());
                }
                else {
                    Toast.makeText(application, response.code(), Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }


            }

            @Override
            public void onFailure(Call<List<CountryModel>> call, Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                data.setValue(null);
            }
        });

        return data;
    }
}
