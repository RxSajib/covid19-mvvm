package com.example.covid19_mvvm.RoomDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.GlobalModel;

import java.util.List;

@Dao
public interface Dio {


    @Query("SELECT * FROM `Country.DB`")
    public LiveData<CountryModel> getcountrydata();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertcountrydata(CountryModel countryModel);

    @Query("DELETE FROM `Country.DB`")
    public void deletecountrydata();

}
