package com.example.covid19_mvvm.RoomDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.GlobalModel;

import java.util.List;

@Dao
public interface GlobalDio {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertglobaldata(List<CountryModel> list);

    @Query("SELECT * FROM `Country.DB`")
    public LiveData<List<CountryModel>> getcallcountrydata();

    @Query("DELETE FROM `Country.DB`")
    public void removealldata();

}
