package com.example.covid19_mvvm.RoomDatabase;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.GlobalModel;

@Database(entities = {CountryModel.class}, version = 2)
public abstract class GlobalDatabase extends RoomDatabase {

    public abstract GlobalDio getglobaldio();
    public static volatile   GlobalDatabase getInstance;


    public static GlobalDatabase getglobaldatabase(final Context context){

        if(getInstance == null){
            synchronized (GlobalDatabase.class){
                if(getInstance == null){
                    getInstance = Room.databaseBuilder(context.getApplicationContext()
                    ,GlobalDatabase.class, "globaldb")
                            .addCallback(callback)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }

        return getInstance;
    }

    static Callback callback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            new FirstRemoveAsyTask(getInstance).execute();

        }
    };

    static class FirstRemoveAsyTask extends AsyncTask<Void, Void, Void>{

        private GlobalDio globalDio;

        public FirstRemoveAsyTask(GlobalDatabase globalDatabase){
            globalDio = globalDatabase.getglobaldio();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            globalDio.removealldata();
            return null;
        }
    }

}
