package com.example.covid19_mvvm.RoomDatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.GlobalModel;

@Database(entities = {CountryModel.class}, version = 5)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {

    public abstract Dio getdio();
    public static volatile RoomDatabase roomDatabase;

    public static RoomDatabase getInstance(final Context context){
        if(roomDatabase == null){
            synchronized (RoomDatabase.class){
                if(roomDatabase == null){
                    roomDatabase = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "country.db")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return roomDatabase;
    }
}
