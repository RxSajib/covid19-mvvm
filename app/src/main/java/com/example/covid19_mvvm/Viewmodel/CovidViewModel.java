package com.example.covid19_mvvm.Viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.covid19_mvvm.Network.Bangladeshrepository;
import com.example.covid19_mvvm.Network.CountrySearch_Repository;
import com.example.covid19_mvvm.Network.CountyRespository;
import com.example.covid19_mvvm.Network.Globalrepository;
import com.example.covid19_mvvm.RoomDatabase.Dio;
import com.example.covid19_mvvm.RoomDatabase.GlobalDatabase;
import com.example.covid19_mvvm.RoomDatabase.GlobalDio;
import com.example.covid19_mvvm.RoomDatabase.RoomDatabase;
import com.example.covid19_mvvm.model.CountryModel;
import com.example.covid19_mvvm.model.GlobalModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CovidViewModel extends AndroidViewModel {

    private Globalrepository globalrepository;
    private Bangladeshrepository bangladeshrepository;
    private CountyRespository countyRespository;
    private CountrySearch_Repository countrySearch_repository;


    private Dio dio;
    private RoomDatabase roomDatabase;

    private GlobalDio globalDio;
    private GlobalDatabase globalDatabase;

    public CovidViewModel(@NonNull @NotNull Application application) {
        super(application);

        globalrepository = new Globalrepository(application);
        bangladeshrepository = new Bangladeshrepository(application);
        countyRespository = new CountyRespository(application);
        countrySearch_repository =  new CountrySearch_Repository(application);


        roomDatabase = RoomDatabase.getInstance(application);
        dio = roomDatabase.getdio();

        globalDatabase = GlobalDatabase.getglobaldatabase(application);
        globalDio = globalDatabase.getglobaldio();
    }

    public LiveData<GlobalModel> getdata(){
      return  globalrepository.getData();
    }

    public LiveData<CountryModel> getbangladeshinfo(){
        return bangladeshrepository.getData();
    }

    public LiveData<List<CountryModel>> getallcountrydata(){
        return countyRespository.getallcountrydata();
    }

    public LiveData<CountryModel> search_country(String name){
       return countrySearch_repository.getsearchdata(name);
    }


    public LiveData<CountryModel> getcountrydata(){
        return dio.getcountrydata();
    }

    public void insertcountrydata(CountryModel countryModel){
        new CountryDataAsyTask(dio).execute(countryModel);
    }

    public void deletecountrydata(){
        new DeleteCountryAsyTask(dio).execute();
    }


    class CountryDataAsyTask extends AsyncTask<CountryModel, Void, Void>{

        private Dio dio;

        public CountryDataAsyTask(Dio dio) {
            this.dio = dio;
        }

        @Override
        protected Void doInBackground(CountryModel... countryModels) {
            dio.insertcountrydata(countryModels[0]);
            return null;
        }
    }

    class DeleteCountryAsyTask extends AsyncTask<Void, Void, Void>{
        private Dio dio;

        public DeleteCountryAsyTask(Dio dio) {
            this.dio = dio;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dio.deletecountrydata();
            return null;
        }
    }


    public void insertglobaldata(List<CountryModel> datalist){
        new GlobalInsertAsyTask(globalDio).execute(datalist);
    }

    class GlobalInsertAsyTask extends AsyncTask<List<CountryModel> , Void, Void>{

        private GlobalDio globalDio;

        public GlobalInsertAsyTask(GlobalDio globalDio) {
            this.globalDio = globalDio;
        }

        @Override
        protected Void doInBackground(List<CountryModel>... lists) {
            globalDio.insertglobaldata(lists[0]);
            return null;
        }
    }


    public LiveData<List<CountryModel>> getalldata(){
        return globalDio.getcallcountrydata();
    }
}
